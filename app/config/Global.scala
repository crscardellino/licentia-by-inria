package config

/**
 * Licentia by INRIA
 * (c) 2014 Cristian Cardellino for INRIA
 *
 * Licensed under Apache License, Version 2.0
 * See LICENSE for more information.
 */

import play.api._
import play.api.libs.concurrent.Execution.Implicits.defaultContext
import play.api.mvc.Results._
import play.api.mvc._
import play.filters.csrf._
import com.mohiva.play.htmlcompressor.HTMLCompressorFilter

import scala.concurrent.Future

object Global extends WithFilters(CSRFFilter(), HTMLCompressorFilter()) with GlobalSettings {
  val dc = "http://purl.org/dc/terms/"
  val l4lod = "http://ns.inria.fr/l4lod/"
  val odrl = "http://www.w3.org/ns/odrl/2/"

  def Prefixes: String = {
    "PREFIX :      <http://wimmics.inria.fr/projects/licentia/licenses/>  " +
    "PREFIX cc:    <http://creativecommons.org/ns#>  " +
    "PREFIX dct:   <http://purl.org/dc/terms/>  " +
    "PREFIX foaf:  <http://xmlns.com/foaf/0.1/>  " +
    "PREFIX l4lod: <http://ns.inria.fr/l4lod/>  " +
    "PREFIX odrl:  <http://www.w3.org/ns/odrl/2/>  " +
    "PREFIX owl:   <http://www.w3.org/2002/07/owl#>  " +
    "PREFIX provo: <http://purl.org/net/provenance/ns#>  " +
    "PREFIX rdf:   <http://www.w3.org/1999/02/22-rdf-syntax-ns#>  " +
    "PREFIX rdfs:  <http://www.w3.org/2000/01/rdf-schema#>  " +
    "PREFIX skos:  <http://www.w3.org/2004/02/skos/core#>  " +
    "PREFIX xml:   <http://www.w3.org/XML/1998/namespace>  " +
    "PREFIX xsd:   <http://www.w3.org/2001/XMLSchema#>  "
  }

  def sanitize(id: String): String = id.replaceAll("[.]", "-")

  override def onError(request: RequestHeader, throwable: Throwable) = {
    Future {
      InternalServerError(views.html.errors.error500())
    }
  }

  override def onHandlerNotFound(request: RequestHeader) = {
    Future {
      NotFound(views.html.errors.error404(request))
    }
  }
}
