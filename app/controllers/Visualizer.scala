package controllers

import play.api.mvc._
import scala.concurrent.ExecutionContext.Implicits.global


object Visualizer extends Controller {
  def visualizerSelection = Action {
    Ok(views.html.visualizerselect())
  }

  def visualize(licenseid: String) = Action.async {
    Licenses.getLicense(licenseid) map { license =>
      Ok(views.html.visualizer(license))
    }
  }
}
