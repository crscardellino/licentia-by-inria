/**
 * Licentia by INRIA
 * (c) 2014 Cristian Cardellino for INRIA
 *
 * Licensed under Apache License, Version 2.0
 * See LICENSE for more information.
 */

package controllers

import com.hp.hpl.jena.query._
import config.Global
import models._
import models.JsonFormats._
import play.api.libs.concurrent.Execution.Implicits.defaultContext
import play.api.mvc._
import play.api.libs.json._
import play.api.Play
import scala.collection.JavaConversions._
import scala.concurrent.Future


object Licenses extends Controller {
  private def queryLicense(license: String) = {
    Global.Prefixes +
    "SELECT DISTINCT ?license ?about ?title ?version " +
    "WHERE { " +
      s"VALUES ?license { :$license } " +
      "?license a odrl:Set . " +
      "?license l4lod:licensingTerms ?about . " +
      "?license rdfs:label ?title . " +
      "OPTIONAL { " +
        "?license dct:hasVersion ?version . " +
      "} " +
    "} "
  }

  private def queryLicenses = {
    Global.Prefixes +
    "SELECT DISTINCT ?license ?about ?title ?version " +
    "( (IF (BOUND(?version), ?version, \"_\") ) AS ?numversion) " +
    "( (IF (STRSTARTS(?title, \"Creative Commons\"), \"1\", \"0\") ) AS ?cc)  " +
    "WHERE { " +
      "?license a odrl:Set . " +
      "?license l4lod:licensingTerms ?about . " +
      "?license rdfs:label ?title . " +
      "OPTIONAL { " +
        "?license dct:hasVersion ?version . " +
      "} " +
    "} ORDER BY ?cc DESC(?numversion) ?title "
  }

  private def queryLicenseActions(license: String, property: String, action: String) = {
    Global.Prefixes +
    "SELECT DISTINCT ?action " +
    "WHERE { " +
      s":$license $property ?b . " +
      s"?b a $action . " +
      "?b odrl:action ?action . " +
    "} "
  }

  def getLicense(licenseid: String): Future[License] = {
    Play.current.configuration.getString("fuseki.uri") match {
      case Some(fusekiuri) => {
        val qexecLicenses = QueryExecutionFactory.sparqlService(s"$fusekiuri/query",
          queryLicense(licenseid))

        val licenses = qexecLicenses.execSelect()

        if (licenses.hasNext) {
          val license = licenses.next()

          Future {
            License(
              licenseid,
              license.getResource("license").getURI,
              license.getResource("about").getURI,
              license.getLiteral("title").getString,
              if (license.getLiteral("version") != null) Some(license.getLiteral("version").getString) else None,
              getLicenseConditions(license.getResource("license").getLocalName).toList
            )
          }
        } else {
          sys.error("Non existing license.")
        }
      }
      case None => sys.error("Fuseki uri not found.")
    }
  }

  def getLicenses: Future[List[License]] = {
    Play.current.configuration.getString("fuseki.uri") match {
      case Some(fusekiuri) => {
        val qexecLicenses = QueryExecutionFactory.sparqlService(s"$fusekiuri/query",
          queryLicenses)

        val licenses = for (license <- qexecLicenses.execSelect()) yield {
          License(
            license.getResource("license").getLocalName,
            license.getResource("license").getURI,
            license.getResource("about").getURI,
            license.getLiteral("title").getString,
            if (license.getLiteral("version") != null) Some(license.getLiteral("version").getString) else None,
            List()
          )
        }

        qexecLicenses.close()

        Future {
          licenses.toList
        }
      }
      case None => sys.error("Fuseki uri not found.")
    }
  }

  def getLicensesWithConditions: Future[List[License]] = {
    Play.current.configuration.getString("fuseki.uri") match {
      case Some(fusekiuri) => {
        val qexecLicenses = QueryExecutionFactory.sparqlService(s"$fusekiuri/query",
            queryLicenses)

        val licenses = for (license <- qexecLicenses.execSelect()) yield {
          License(
            license.getResource("license").getLocalName,
            license.getResource("license").getURI,
            license.getResource("about").getURI,
            license.getLiteral("title").getString,
            if (license.getLiteral("version") != null) Some(license.getLiteral("version").getString) else None,
            getLicenseConditions(license.getResource("license").getLocalName).toList
          )
        }

        qexecLicenses.close()

        Future { licenses.toList }
      }
      case None => sys.error("Fuseki uri not found.")
    }
  }

  def getLicenseConditions(license: String): List[Condition] = {
    Play.current.configuration.getString("fuseki.uri") match {
      case Some(fusekiuri) => {
        var qexec = QueryExecutionFactory.sparqlService(s"$fusekiuri/query",
          queryLicenseActions(license, "odrl:permission", "odrl:Permission")
        )

        val permissions: Iterator[Condition] = for (result <- qexec.execSelect()) yield {
          Condition(
            "permission",
            result.getResource("action").getLocalName,
            None
          )
        }

        qexec.close()

        qexec = QueryExecutionFactory.sparqlService(s"$fusekiuri/query",
          queryLicenseActions(license, "odrl:duty", "odrl:Duty")
        )

        val requirements: Iterator[Condition] = for (result <- qexec.execSelect()) yield {
          Condition(
            "requirement",
            result.getResource("action").getLocalName,
            None
          )
        }

        qexec.close()

        qexec = QueryExecutionFactory.sparqlService(s"$fusekiuri/query",
          queryLicenseActions(license, "odrl:prohibition", "odrl:Prohibition")
        )

        val prohibitions: Iterator[Condition] = for (result <- qexec.execSelect()) yield {
          Condition(
            "prohibition",
            result.getResource("action").getLocalName,
            None
          )
        }

        qexec.close()

        (permissions ++ requirements ++ prohibitions).toList
      }
      case None => sys.error("Fuseki uri not found.")
    }
  }

  def listLicenses = Action.async {
    getLicenses map {
      licenses =>
        Ok(Json.arr(licenses)(0))
    }
  }
}

