/******************************************************************************
*   Licentia by INRIA                                                         *
*                                                                             *
*   (c) 2014 Cristian Cardellino for INRIA                                    *
*                                                                             *
*   Licensed under the Apache License, Version 2.0 (the "License");           *
*   you may not use this file except in compliance with the License.          *
*   You may obtain a copy of the License at                                   *
*                                                                             *
*       http://www.apache.org/licenses/LICENSE-2.0                            *
*                                                                             *
*   Unless required by applicable law or agreed to in writing, software       *
*   distributed under the License is distributed on an "AS IS" BASIS,         *
*   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  *
*   See the License for the specific language governing permissions and       *
*   limitations under the License.                                            *
*                                                                             *
******************************************************************************/

package controllers

import forms.LicenseData
import models._
import play.api.cache.Cache
import play.api.data._
import play.api.data.Forms._
import play.api.libs.json.Json
import play.api.mvc._
import play.api.libs.concurrent.Execution.Implicits.defaultContext
import play.api.Play.current
import play.filters.csrf._
import scala.collection.mutable.{ListBuffer, Map=>Dict}
import scala.concurrent.Future


object LicenseService extends Controller {

  val licenseForm = Form(
    mapping(
      "requirements" -> list(text),
      "permissions" -> list(text),
      "prohibitions" -> list(text)
    )(LicenseData.apply)(LicenseData.unapply)
  )

  private def compatibilityChecker(userConditions: List[Condition], licenses: List[License],
                                   incompatibleLicenses: Dict[String, Set[Condition]]):
                                   List[(License, Set[Condition])] = {
    val compatibleLicenses: ListBuffer[(License, Set[Condition])] = ListBuffer()

    for (license <- licenses) {
      val conditionsCompatibilityChecker: ConditionsCompatibilityChecker =
        new ConditionsCompatibilityChecker(userConditions, license)

      val conditionsStatus = conditionsCompatibilityChecker.getConditionsCompatibility

      if (conditionsStatus._1) compatibleLicenses += ((license, conditionsStatus._2))
      else incompatibleLicenses(license.id) = conditionsStatus._2
    }

    compatibleLicenses.result()
  }

  private def getUserConditions(requirements: List[String], permissions: List[String],
                                prohibitions: List[String]): List[Condition] = {
    val userConditions: ListBuffer[Condition] = ListBuffer()

    userConditions ++= (for (condition <- requirements) yield Condition("requirement", condition, None))
    userConditions ++= (for (condition <- permissions) yield Condition("permission", condition, None))
    userConditions ++= (for (condition <- prohibitions) yield Condition("prohibition", condition, None))

    userConditions.result()
  }

  def licenseServiceIndex = CSRFAddToken {
    Action { implicit request =>
      request.session.get("uuid") match {
        case Some(uuid) =>
          Cache.getAs[List[Condition]] (s"$uuid.license.service.conditions") match {
            case Some (conditions) =>
              Cache.remove(s"$uuid.license.service.conditions")
              Ok(views.html.licenseservice(conditions))
            case None =>
              Ok(views.html.licenseservice())
          }
        case None =>
          val uuid = java.util.UUID.randomUUID.toString
          Redirect(routes.LicenseService.licenseServiceIndex()).withSession(request.session + ("uuid" -> uuid))
      }
    }
  }

  def checkLicenseCompatibility = CSRFCheck { Action.async { implicit request =>
    val licensesFuture: Future[List[License]] = Licenses.getLicensesWithConditions

    licensesFuture map {
      licenses => {
        val licenseData = licenseForm.bindFromRequest.get

        val userConditions = getUserConditions(licenseData.requirements.toSet.toList,
          licenseData.permissions.toSet.toList,
          licenseData.prohibitions.toSet.toList)

        var uuid: String = ""

        request.session.get("uuid") match {
          case Some(suuid) => uuid = suuid
          case None => uuid = java.util.UUID.randomUUID.toString
        }

        Cache.set(s"$uuid.license.service.conditions", userConditions, 600)

        val incompatibleLicenses: Dict[String, Set[Condition]] = Dict()
        val compatibleLicenses: List[(License, Set[Condition])] = compatibilityChecker(userConditions, licenses, incompatibleLicenses)

        if (compatibleLicenses.nonEmpty) {
          val perfectMatches = for (l <- compatibleLicenses if l._2.isEmpty) yield {
            (l._1, l._1.conditions.toSet -- userConditions.toSet)
          }
          val matches = compatibleLicenses filter (_._2.nonEmpty)

          Ok(views.html.licenseresults(
            perfectMatches.sortBy(m => (m._2.size, m._1.conditions.length)),
            matches.sortBy(m => (m._2.size, m._1.conditions.length)),
            userConditions.toSet.toList.sorted
          )).withSession(request.session + ("uuid" -> uuid))
        }
        else {
          val validIncompleteLicenses = incompatibleLicenses.filter(_._2.size < userConditions.size)

          val incompleteLicenses = for (license <- licenses.toList
                                        if validIncompleteLicenses contains license.id) yield
                                        (license, validIncompleteLicenses(license.id))

          Ok(views.html.incompatiblelicenseresults(
            incompleteLicenses.sortBy(l => (l._2.size, l._1.conditions.length)),
            userConditions.toSet.toList.sorted
          )).withSession(request.session + ("uuid" -> uuid))
        }
      }
    }
  }}
}
