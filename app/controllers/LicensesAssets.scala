/**
 * Licentia by INRIA
 * (c) 2014 Cristian Cardellino for INRIA
 *
 * Licensed under Apache License, Version 2.0
 * See LICENSE for more information.
 */

package controllers

import java.io._
import play.api.mvc._


object LicensesAssets extends Controller {
  def serve(filename: String) = Action { implicit request =>
    try {
      Ok.sendFile(
        content = new File(s"/var/www/licentia/licenses/$filename"),
        inline = true
      )
    } catch {
      case e: FileNotFoundException => NotFound(views.html.errors.error404())
    }
  }
}
