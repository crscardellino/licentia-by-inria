/**
 * Licentia by INRIA
 * (c) 2014 Cristian Cardellino for INRIA
 *
 * Licensed under Apache License, Version 2.0
 * See LICENSE for more information.
 */

package controllers

import com.hp.hpl.jena.query._
import config.Global
import models._
import models.JsonFormats._
import play.api.Play
import play.api.libs.concurrent.Execution.Implicits.defaultContext
import play.api.mvc._
import play.api.libs.json._
import scala.collection.JavaConversions._
import scala.concurrent.Future


object Conditions extends Controller {
  private def query(action: String) = {
    Global.Prefixes +
    "SELECT DISTINCT ?action ?label " +
    "WHERE { " +
      s"?condition a odrl:$action. " +
      "?condition odrl:action ?action. " +
      "OPTIONAL { " +
        "?action a odrl:Action. " +
        "?action rdfs:label ?label . " +
      "}. " +
    "} "
  }

  private def getConditions: Future[Iterator[Condition]] = {
    Play.current.configuration.getString("fuseki.uri") match {
      case Some(fusekiuri) => {
        var queryexec = QueryExecutionFactory.sparqlService(s"$fusekiuri/query", query("Permission"))

        val permissions: Iterator[Condition] = for (result <- queryexec.execSelect()) yield {
          var label: String = ""

          if (result.getLiteral("label") != null) {
            label = result.getLiteral("label").getString
          } else {
            label = result.getResource("action").getLocalName.capitalize
          }

          Condition(
            "permission",
            result.getResource("action").getLocalName,
            Some(label)
          )
        }

        queryexec.close()

        queryexec = QueryExecutionFactory.sparqlService(s"$fusekiuri/query", query("Duty"))

        val requirements: Iterator[Condition] = for (result <- queryexec.execSelect()) yield {
          var label: String = ""

          if (result.getLiteral("label") != null) {
            label = result.getLiteral("label").getString
          } else {
            label = result.getResource("action").getLocalName.capitalize
          }

          Condition("requirement",
            result.getResource("action").getLocalName,
            Some(label)
          )
        }

        queryexec.close()

        queryexec = QueryExecutionFactory.sparqlService(s"$fusekiuri/query", query("Prohibition"))

        val prohibitions: Iterator[Condition] = for (result <- queryexec.execSelect()) yield {
          var label: String = ""

          if (result.getLiteral("label") != null) {
            label = result.getLiteral("label").getString
          } else {
            label = result.getResource("action").getLocalName.capitalize
          }

          Condition("prohibition",
            result.getResource("action").getLocalName,
            Some(label)
          )
        }

        queryexec.close()

        Future { permissions ++ requirements ++ prohibitions }
      }
      case None => sys.error("Fuseki uri not found.")
    }
  }

  def listConditions = Action.async(
    getConditions map {
      conditions =>
        Ok(Json.arr(conditions.toList)(0))
    }
  )
}