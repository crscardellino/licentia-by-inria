/**
 * Licentia by INRIA
 * (c) 2014 Cristian Cardellino for INRIA
 *
 * Licensed under Apache License, Version 2.0
 * See LICENSE for more information.
 */

package controllers

import play.api.mvc._
import play.api.mvc.Results._

import play.filters.csrf._

trait Authenticate {
  def checkValidRequest(request: Request[Any]): Boolean = {
    request.session.get("connected") match {
      case Some(username) if username == "admin" => {
        request.session.get("timestamp") match {
          case Some(timestamp) => {
            if(System.currentTimeMillis - timestamp.toLong < 1800000)
              return true
          }
          case None =>
        }
      }
      case _ =>
    }

    false
  }

  def AuthenticateUser(result: Request[AnyContent] => Result) = CSRFAddToken {
    Action { implicit request =>
      if(checkValidRequest(request))
        result(request)
      else
        Ok(views.html.login(request.path))
    }
  }
}
