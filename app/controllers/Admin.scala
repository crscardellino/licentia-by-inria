/******************************************************************************
*   Licentia by INRIA                                                         *
*                                                                             *
*   (c) 2014 Cristian Cardellino for INRIA                                    *
*                                                                             *
*   Licensed under the Apache License, Version 2.0 (the "License");           *
*   you may not use this file except in compliance with the License.          *
*   You may obtain a copy of the License at                                   *
*                                                                             *
*       http://www.apache.org/licenses/LICENSE-2.0                            *
*                                                                             *
*   Unless required by applicable law or agreed to in writing, software       *
*   distributed under the License is distributed on an "AS IS" BASIS,         *
*   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  *
*   See the License for the specific language governing permissions and       *
*   limitations under the License.                                            *
*                                                                             *
******************************************************************************/

package controllers

import com.hp.hpl.jena.rdf.model._
import com.hp.hpl.jena.query.DatasetAccessorFactory
import com.hp.hpl.jena.vocabulary.RDF
import config.Global
import forms._
import java.io._
import org.mindrot.jbcrypt.BCrypt
import play.api.data._
import play.api.data.Forms._
import play.api.libs.json._
import play.api.libs.concurrent.Execution.Implicits.defaultContext
import play.api.mvc._
import play.api.Play
import play.filters.csrf._
import scala.concurrent.Future


object Admin extends Controller with Authenticate {

  val loginForm = Form(
    mapping(
      "username" -> nonEmptyText,
      "password" -> nonEmptyText,
      "next" -> text
    )(LoginData.apply)(LoginData.unapply)
  )

  val licenseForm = Form(
    mapping(
      "urn" -> nonEmptyText,
      "about" -> nonEmptyText,
      "title" -> nonEmptyText,
      "requirements" -> list(text),
      "permissions" -> list(text),
      "prohibitions" -> list(text)
    )(UploadLicense.apply)(UploadLicense.unapply)
  )

  private def existingLicenseURI(licenseuri: String): Future[Boolean] = {
    Licenses.getLicenses map {
      licenses =>
        (for (license <- licenses) yield license.uri) contains licenseuri
    }
  }

  def login = AuthenticateUser { implicit request =>
    Ok(views.html.adminindex())
  }

  def logout = Action {
    Ok(views.html.index()).withNewSession
  }

  def admin = CSRFCheck {
    Action { implicit request =>
      loginForm.bindFromRequest.fold(
        errorForm => {
          Forbidden(views.html.errors.error403()).withNewSession
        },
        loginData => {
          val hashpwOpt = Play.current.configuration.getString("application.hashpw")

          hashpwOpt match {
            case Some(hashpw) => {
              if (loginData.username == "admin" && BCrypt.checkpw(loginData.password, hashpw)) {
                if (loginData.next.trim.isEmpty)
                  Ok(views.html.adminindex()).withSession(request.session +
                      ("connected" -> "admin") +
                      ("timestamp" -> System.currentTimeMillis.toString))
                else
                  Redirect(loginData.next).withSession(request.session +
                      ("connected" -> "admin") +
                      ("timestamp" -> System.currentTimeMillis.toString))
              }
              else
                Forbidden(views.html.errors.error403("Either username or password was invalid.")).withNewSession
            }
            case None => sys.error("Admin hashed password not set.")
          }
        }
      )
    }
  }

  def newlicense = CSRFAddToken {
    AuthenticateUser { implicit request =>
      Ok(views.html.newlicense())
    }
  }

  def uploadlicense = CSRFCheck {
    Action.async(parse.multipartFormData) { implicit request =>
      if(checkValidRequest(request)) {
        request.body.file("rdffile").map { file =>
          val in = new FileInputStream(file.ref.file)
          val model = ModelFactory.createDefaultModel()
          val licenseType = ResourceFactory.createResource(s"${Global.odrl}Set")

          try {
            model.read(in, null, "TTL")
          } catch {
            case ex: Exception => Future { Ok("incorrect") }
          }

          val validlicense = model.listSubjectsWithProperty(RDF.`type`, licenseType)

          if (validlicense.hasNext) {
            val license = validlicense.nextResource()

            existingLicenseURI(license.getURI) map {
              value =>
                if (value) {
                  Ok(views.html.newlicense("existing"))
                } else {
                  val titleProperty = ResourceFactory.createProperty(s"${Global.dc}title")
                  val termsProperty = ResourceFactory.createProperty(s"${Global.l4lod}licensingTerms")

                  if (!license.hasProperty(titleProperty)) {
                    Ok(views.html.newlicense("missing-title"))
                  } else if (!license.hasProperty(termsProperty)) {
                    Ok(views.html.newlicense("missing-terms"))
                  } else {
                    Play.current.configuration.getString("fuseki.uri") match {
                      case Some(fusekiuri) =>
                        val dts = DatasetAccessorFactory.createHTTP(s"$fusekiuri/data")
                        dts.add(model)

                        val filename = s"${license.getLocalName}.ttl"
                        val contentType = "text/turtle"
                        file.ref.moveTo(new File(s"/var/www/licentia/licenses/$filename"), true)

                        play.api.Logger.info(s"License $filename uploaded.")

                        Ok(views.html.newlicense("created"))

                      case None => sys.error("Fuseki uri not found.")
                    }
                  }
                }
            }
          } else {
            Future { Ok(views.html.newlicense("incorrect")) }
          }
        }.getOrElse{
          Future { Ok(views.html.newlicense("missing")) }
        }
      }
      else Future { Forbidden(views.html.errors.error403()) }
    }
  }
}