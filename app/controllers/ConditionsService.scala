/******************************************************************************
*   Licentia by INRIA                                                         *
*                                                                             *
*   (c) 2014 Cristian Cardellino for INRIA                                    *
*                                                                             *
*   Licensed under the Apache License, Version 2.0 (the "License");           *
*   you may not use this file except in compliance with the License.          *
*   You may obtain a copy of the License at                                   *
*                                                                             *
*       http://www.apache.org/licenses/LICENSE-2.0                            *
*                                                                             *
*   Unless required by applicable law or agreed to in writing, software       *
*   distributed under the License is distributed on an "AS IS" BASIS,         *
*   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  *
*   See the License for the specific language governing permissions and       *
*   limitations under the License.                                            *
*                                                                             *
******************************************************************************/

package controllers

import forms.ConditionsData
import models._
import play.api.cache.Cache
import play.api.mvc._
import play.api.data._
import play.api.data.Forms._
import play.api.libs.concurrent.Execution.Implicits.defaultContext
import play.api.Play.current
import play.filters.csrf._
import scala.collection.mutable.ListBuffer
import scala.concurrent.Future


object ConditionsService extends Controller {
  val conditionsForm = Form(
    mapping(
      "license" -> text,
      "requirements" -> list(text),
      "permissions" -> list(text),
      "prohibitions" -> list(text)
    )(ConditionsData.apply)(ConditionsData.unapply)
  )

  private def getUserConditions(requirements: List[String], permissions: List[String],
                                prohibitions: List[String]): List[Condition] = {
    val userConditions: ListBuffer[Condition] = ListBuffer()

    userConditions ++= (for (condition <- requirements) yield Condition("requirement", condition, None))
    userConditions ++= (for (condition <- permissions) yield Condition("permission", condition, None))
    userConditions ++= (for (condition <- prohibitions) yield Condition("prohibition", condition, None))

    userConditions.result()
  }

  def conditionsServiceIndex = CSRFAddToken {
    Action { implicit request =>
      request.session.get("uuid") match {
        case Some(uuid) =>
          Cache.getAs[(String, List[Condition])](s"$uuid.condition.service.conditions") match {
            case Some((license, conditions)) =>
              Cache.remove(s"$uuid.condition.service.conditions")
              Ok(views.html.conditionsservice(license, conditions))
            case None => Ok(views.html.conditionsservice())
          }
        case None =>
          val uuid = java.util.UUID.randomUUID.toString
          Redirect(routes.ConditionsService.conditionsServiceIndex()).withSession(request.session + ("uuid" -> uuid))
      }
    }
  }

  def checkConditionsCompatibility = CSRFCheck {
    Action.async { implicit request =>
      val data = conditionsForm.bindFromRequest.get

      val licenses: Future[License] = Licenses.getLicense(data.license)

      licenses map {
        license =>
          val userConditions = getUserConditions(data.requirements, data.permissions, data.prohibitions)

          var uuid: String = ""

          request.session.get("uuid") match {
            case Some(suuid) => uuid = suuid
            case None => uuid = java.util.UUID.randomUUID.toString
          }

          Cache.set(s"$uuid.condition.service.conditions", (license.id, userConditions), 600)

          val conditionsCompatibilityChecker =
            new ConditionsCompatibilityChecker(userConditions, license)

          val conditionsStatus = conditionsCompatibilityChecker.getConditionsCompatibility

          var unmetConditions: Set[Condition] = Set()

          if (conditionsStatus._1 | conditionsStatus._2.size <= userConditions.toSet.size)
            unmetConditions = conditionsStatus._2

          Ok(views.html.conditionsresults(
              conditionsStatus._1,
              license.title,
              unmetConditions)
          ).withSession(request.session + ("uuid" -> uuid))
      }
    }
  }
}
