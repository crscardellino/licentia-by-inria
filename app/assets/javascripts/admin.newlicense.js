/*
 * Licentia by INRIA
 * (c) 2014 Cristian Cardellino for INRIA
 *
 * Licensed under Apache License, Version 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for more information.
 */

$("#new-license-form").submit(function (event) {
  event.preventDefault();

  $(".msg-form").addClass("hide");

  $("#rdffile").parent().removeClass("has-error");

  $("#form-submit").empty();
  $("#form-submit").html('<i class="fa fa-spinner fa-spin fa-lg"></i>');
  $("#form-submit").attr("disabled", "disabled");

  if ($("#rdffile").val().trim() === "") {
    $("#rdffile").parent().addClass("has-error");
    $("#missing").removeClass("hide");
    $("#form-submit").empty();
    $("#form-submit").html('Upload New License');
    $("#form-submit").removeAttr("disabled");
  } else {
    $("#new-license-form").unbind('submit').submit();
  }
});