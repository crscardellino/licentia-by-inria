/*
 * Licentia by INRIA
 * (c) 2014 Cristian Cardellino for INRIA
 *
 * Licensed under Apache License, Version 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for more information.
 */

function sanitize(id) {
  return id.replace(/\./g, "-");
}

function newitem(id, label, clazz, value) {
  return '<li class="list-group-item ' + clazz + "-item" +
          '" id="' + id + '" data-value="' + value + '">' + label +
          ' <a href="#" id="remove-' + id + '"><i class="fa fa-times text-danger"></i></a></li>';
}

function newemptyitem(id, clazz) {
  return '<li class="list-group-item ' + clazz + "-item" +
            '" id="' + id + '" data-value="null">&nbsp;</li>';
}

function addCondition(value, label, clazz) {
  var items = $("." + clazz + "-item").map(function(){return $(this).attr("data-value");}).get();

  if (value === null || items.indexOf(value) > -1) {
    return false;
  }

  var counter = 0;

  if (clazz == "permission") {
    counter = conditionsCounter.permissions++;
  } else if (clazz == "requirement") {
    counter = conditionsCounter.requirements++;
  } else if (clazz == "prohibition") {
    counter = conditionsCounter.prohibitions++;
  }

  var id = clazz + "s_" + counter;
  var name = clazz + "s[" + counter + "]";

  var item = newitem(id, label, clazz, value);

  if ($("." + clazz + "-item").length == 2 && $("." + clazz + "-item").last().attr("data-value") === "null") {
    $("." + clazz + "-item").last().replaceWith(item);
  } else {
    $("." + clazz + "-item").last().after(item);
  }

  $("<input>").attr({
    type: "hidden",
    id: id + "-hidden",
    name: name,
    value: value,
    class: clazz + "-hidden"
  }).appendTo("#main-form");

  $("#remove-" + id).click(function(event) {
    event.preventDefault();

    var selectorid = "#" + clazz + "s-selector";

    $(selectorid).find("option[value=" + value + "]").removeAttr("disabled");

    if($(selectorid).val() == value) {
      $(selectorid).find(":first-child").prop("selected", true);
    }


    if ($("." + clazz + "-item").length == 2) {
      var emptyitem = newemptyitem(clazz + "-blank", clazz);

      $("#" + id).replaceWith(emptyitem);
    } else {
      $("#" + id).remove();
    }

    $("#" + id + "-hidden").remove();
  });
}

function newitemlicense(id, title) {
  return '<li class="list-group-item license-item"' +
    ' id="' + id + '">' + title +
    ' <a href="#" id="remove-' + id + '"><i class="fa fa-times text-danger"></i></a></li>';
}

function changeLicense(value, title) {
  if (value === null) {
    return false;
  }
  $("#license-hidden").remove();

  var id = sanitize(value);
  var name = "license";

  var item = newitemlicense(id, title);

  $(".license-item").last().replaceWith(item);

  $("<input>").attr({
    type: "hidden",
    id: "license-hidden",
    name: name,
    value: value
  }).appendTo("#main-form");

  $("#remove-" + id).click(function(event) {
    event.preventDefault();

    if($("#licenses-selector").val() == value) {
      $("#licenses-selector").find(":first-child").prop("selected", true);
    }

    var emptyitem = newemptyitem("license-blank", "license");

    $("#" + id).replaceWith(emptyitem);

    $("#license-hidden").remove();
  });
}

$(window).load(function() {
  var permissions = [];
  var requirements = [];
  var prohibitions = [];

  var template = null;
  var html = null;

  if (typeof apiLicensesPath != "undefined" && apiLicensesPath !== "") {
    $.get(apiLicensesPath, function(licenses) {
      licenses.sort(sortLicenses);

      var licensesContext = {options: licenses};
      template = Handlebars.compile($("#selector-template").html());
      html = template(licensesContext);
      $("#licenses-selector").append(html);

      if (typeof oldLicense != "undefined" && oldLicense !== "") {
        var title = $.grep(licenses, function(l){return l.id == oldLicense;})[0].title;

        changeLicense(oldLicense, title);
      }

      $("#licenses-selector").change(function () {
        $("#license-error-message").addClass("hidden");
        $("#licenses-input-group").removeClass("has-error");
        if($("#license-blank").length !== 0) {
          $("#license-blank").removeClass("incorrect-condition");
        }

        var value = $("#licenses-selector").val();
        var title = $("#licenses-selector").find(":selected").text();

        changeLicense(value, title);
      });

      if (queryDone) {
        $("#loader").remove();
        $("#service").removeClass("hidden");
      } else {
        queryDone = true;
      }
    });
  }

  $.get(apiConditionsPath, function(conditions) {
    $.each(conditions, function (index, condition) {
      if(condition.clazz == "permission")
        permissions.push(condition);
      else if(condition.clazz == "requirement")
        requirements.push(condition);
      else if(condition.clazz == "prohibition")
        prohibitions.push(condition);
    });

    permissions.sort(sortConditions);
    requirements.sort(sortConditions);
    prohibitions.sort(sortConditions);

    var permissionsContext = {options: permissions};
    template = Handlebars.compile($("#selector-template").html());
    html = template(permissionsContext);
    $("#permissions-selector").append(html);

    var requirementsContext = {options: requirements};
    template = Handlebars.compile($("#selector-template").html());
    html = template(requirementsContext);
    $("#requirements-selector").append(html);

    var prohibitionsContext = {options: prohibitions};
    template = Handlebars.compile($("#selector-template").html());
    html = template(prohibitionsContext);
    $("#prohibitions-selector").append(html);

    var label = "";

    if(typeof oldConditions != "undefined") {
      $.each(oldConditions, function(index, condition) {
        console.log("#" + condition.clazz + "s-selector");

        $("#" + condition.clazz + "s-selector").find("option[value='" + condition.rule + "']").attr("disabled", "disabled");

        if (condition.clazz == "permission") {
          label = $.grep(permissions, function(c){return c.rule == condition.rule;})[0].label;

          addCondition(condition.rule, label, condition.clazz);
        }
        else if (condition.clazz == "requirement") {
          label = $.grep(requirements, function(c){return c.rule == condition.rule;})[0].label;

          addCondition(condition.rule, label, condition.clazz);
        }
        else if (condition.clazz == "prohibition") {
          label = $.grep(prohibitions, function(c){return c.rule == condition.rule;})[0].label;

          addCondition(condition.rule, label, condition.clazz);
        }
      });
    }

    if (typeof apiLicensesPath == "undefined" || apiLicensesPath === "") {
      $("#loader").remove();
      $("#service").removeClass("hidden");
    } else if (queryDone) {
      $("#loader").remove();
      $("#service").removeClass("hidden");
    } else {
      queryDone = true;
    }
  });

  if(typeof apiLicensesPath == "undefined" || apiLicensesPath === "") {
    $("#permissions-tab").click(function(event) {
      $("#permissions-items").addClass("list-head-permissions");
      $("#requirements-items").removeClass("list-head-requirements");
      $("#prohibitions-items").removeClass("list-head-prohibitions");

      $("#previous").attr("disabled", "disabled");
      $("#next").removeAttr("disabled");
      return true;
    });

    $("#requirements-tab").click(function(event) {
      $("#permissions-items").removeClass("list-head-permissions");
      $("#requirements-items").addClass("list-head-requirements");
      $("#prohibitions-items").removeClass("list-head-prohibitions");

      $("#previous").removeAttr("disabled");
      $("#next").removeAttr("disabled");
      return true;
    });

    $("#prohibitions-tab").click(function(event) {
      $("#permissions-items").removeClass("list-head-permissions");
      $("#requirements-items").removeClass("list-head-requirements");
      $("#prohibitions-items").addClass("list-head-prohibitions");

      $("#previous").removeAttr("disabled");
      $("#next").attr("disabled", "disabled");
      return true;
    });

    $("#previous").click(function(event) {
      if ($("#prohibitions").hasClass("active")) {
        $("#requirements-tab").click();
      } else {
        $("#permissions-tab").click();
      }
    });

    $("#next").click(function(event) {
      if ($("#permissions").hasClass("active")) {
        $("#requirements-tab").click();
      } else {
        $("#prohibitions-tab").click();
      }
    });

    $("#permissions-items").click(function(event) {
      $("#permissions-tab").click();
    });

    $("#requirements-items").click(function(event) {
      $("#requirements-tab").click();
    });

    $("#prohibitions-items").click(function(event) {
      $("#prohibitions-tab").click();
    });
  } else {
    $("#licenses-tab").click(function (event){
      $("#licenses-items").addClass("list-head-licenses");
      $("#permissions-items").removeClass("list-head-permissions");
      $("#requirements-items").removeClass("list-head-requirements");
      $("#prohibitions-items").removeClass("list-head-prohibitions");

      $("#previous").attr("disabled", "disabled");
      $("#next").removeAttr("disabled");
      return true;
    });

    $("#permissions-tab").click(function(event) {
      $("#licenses-items").removeClass("list-head-licenses");
      $("#permissions-items").addClass("list-head-permissions");
      $("#requirements-items").removeClass("list-head-requirements");
      $("#prohibitions-items").removeClass("list-head-prohibitions");

      $("#previous").removeAttr("disabled");
      $("#next").removeAttr("disabled");
      return true;
    });

    $("#requirements-tab").click(function(event) {
      $("#licenses-items").removeClass("list-head-licenses");
      $("#permissions-items").removeClass("list-head-permissions");
      $("#requirements-items").addClass("list-head-requirements");
      $("#prohibitions-items").removeClass("list-head-prohibitions");

      $("#previous").removeAttr("disabled");
      $("#next").removeAttr("disabled");
      return true;
    });

    $("#prohibitions-tab").click(function(event) {
      $("#licenses-items").removeClass("list-head-licenses");
      $("#permissions-items").removeClass("list-head-permissions");
      $("#requirements-items").removeClass("list-head-requirements");
      $("#prohibitions-items").addClass("list-head-prohibitions");

      $("#previous").removeAttr("disabled");
      $("#next").attr("disabled", "disabled");
      return true;
    });

    $("#previous").click(function(event) {
      if ($("#prohibitions").hasClass("active")) {
        $("#requirements-tab").click();
      } else if ($("#requirements").hasClass("active")) {
        $("#permissions-tab").click();
      } else {
        $("#licenses-tab").click();
      }
    });

    $("#next").click(function(event) {
      if ($("#licenses").hasClass("active")) {
        $("#permissions-tab").click();
      } else if ($("#permissions").hasClass("active")) {
        $("#requirements-tab").click();
      } else {
        $("#prohibitions-tab").click();
      }
    });

    $("#licenses-items").click(function(event) {
      $("#licenses-tab").click();
    });

    $("#permissions-items").click(function(event) {
      $("#permissions-tab").click();
    });

    $("#requirements-items").click(function(event) {
      $("#requirements-tab").click();
    });

    $("#prohibitions-items").click(function(event) {
      $("#prohibitions-tab").click();
    });
  }
});

$("#permissions-selector").change(function(event) {
  var value = $("#permissions-selector").val();
  var label = $("#permissions-selector").find(":selected").text();

  $("#permissions-selector").find(":selected").attr("disabled", "disabled");

  addCondition(value, label, "permission");
});

$("#requirements-selector").change(function(event) {
  var value = $("#requirements-selector").val();
  var label = $("#requirements-selector").find(":selected").text();

  $("#requirements-selector").find(":selected").attr("disabled", "disabled");

  addCondition(value, label, "requirement");
});

$("#prohibitions-selector").change(function(event) {
  var value = $("#prohibitions-selector").val();
  var label = $("#prohibitions-selector").find(":selected").text();

  $("#prohibitions-selector").find(":selected").attr("disabled", "disabled");

  addCondition(value, label, "prohibition");
});

$("#main-form").submit(function(event) {
  $(".incorrect-condition").removeClass("incorrect-condition");
  $("#form-submit").empty();
  $("#form-submit").html('<i class="fa fa-spinner fa-spin fa-lg"></i>');
  $("#form-submit").attr("disabled", "disabled");

  if(typeof apiLicensesPath != "undefined" && apiLicensesPath !== "") {
    if ($("#license-hidden").length !== 1) {
      event.preventDefault();

      $("#license-error-message").removeClass("hidden");
      $("#licenses-input-group").addClass("has-error");
      if($("#license-blank").length !== 0) {
        $("#license-blank").addClass("incorrect-condition");
      }
      $("#licenses-tab").click();
    }
  }

  var selectedPermissions = [];
  var selectedProhibitions = [];

  $.each($(".permission-hidden"), function(index, hidden) {
    selectedPermissions.push({
      "id": hidden.id,
      "rule": hidden.value
    });
  });

  $.each($(".prohibition-hidden"), function(index, hidden) {
    selectedProhibitions.push({
      "id": hidden.id,
      "rule": hidden.value
    });
  });

  var intersect = [];

  $.each(selectedPermissions, function(iperm, permission) {
    $.each(selectedProhibitions, function(ipro, prohibition) {
      if(permission.rule == prohibition.rule) {
        intersect.push(permission.id);
        intersect.push(prohibition.id);
      }
    });
  });

  if(intersect.length > 0) {
    event.preventDefault();

    $.each(intersect, function (index, id) {
      var sid = id.replace("-hidden", "");
      $("#" + sid).addClass("incorrect-condition");
    });

    $("#form-submit").empty();
    $("#form-submit").html(buttonLabel);
    $("#form-submit").removeAttr("disabled");
    $("#incorrect-settings").modal();
  } else {
    $("#form-submit").empty();
    $("#form-submit").html(buttonLabel);
    $("#form-submit").removeAttr("disabled");
    return true;
  }
});
