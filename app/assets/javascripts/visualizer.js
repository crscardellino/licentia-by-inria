var nodes = [
  { id: "id", label: license.id, level: 0, mass: 10 },
  { id: "title", label: license.title, level: 1, mass: 5 },
  { id: "about", label: license.about, level: 1, mass: 5 }
];

var edges = [
  { from: "id", to: "title", label: "rdfs:label" },
  { from: "id", to: "about", label: "l4lod:licensingTerms" }
];

if (license.permissions.length) {
  nodes.push({ id: "permission", label: "odrl:Permission", level: 1, mass: 5, group: "permissions" });

  edges.push({ from: "id", to: "permission", label: "odrl:permission" });

  for(var i = 0; i < license.permissions.length; i++) {
    nodes.push({
      id: "permission" + i,
      label: "odrl:" + license.permissions[i],
      level: 2,
      mass: 5,
      group: "permissions"
    });

    edges.push({
      from: "permission",
      to: "permission" + i,
      label: "odrl:action"
    });
  }
}

if (license.requirements.length) {
  nodes.push({ id: "requirement", label: "odrl:Duty", level: 1, mass: 5, group: "requirements" });

  edges.push({ from: "id", to: "requirement", label: "odrl:duty" });

  for(var i = 0; i < license.requirements.length; i++) {
    nodes.push({
      id: "requirement" + i,
      label: "odrl:" + license.requirements[i],
      level: 2,
      mass: 5,
      group: "requirements"
    });

    edges.push({
      from: "requirement",
      to: "requirement" + i,
      label: "odrl:action"
    });
  }
}

if (license.prohibitions.length) {
  nodes.push({ id: "prohibition", label: "odrl:Prohibition", level: 1, mass: 5, group: "prohibitions" });

  edges.push({ from: "id", to: "prohibition", label: "odrl:prohibition" });

  for(var i = 0; i < license.prohibitions.length; i++) {
    nodes.push({
      id: "prohibition" + i,
      label: "odrl:" + license.prohibitions[i],
      level: 2,
      mass: 5,
      group: "prohibitions"
    });

    edges.push({
      from: "prohibition",
      to: "prohibition" + i,
      label: "odrl:action"
    });
  }
}

var container = $("#graph").get(0);
var data= {
  nodes: nodes,
  edges: edges,
};
var options = {
  color: 'grey',
  height: '500px',
  smoothCurves: true,
  stabilize: true,
  stabilizationIterations: 5000,
  width: '100%',
  nodes: {
    shape: "box"
  },
  edges: {
    fontFill: "#f5f5f5",
    inheritColor: "to"
  },
  groups: {
    permissions: {
      color: {
        border: "#18bc9c",
        background: "#18bc9c",
        highlight: {
          border: "#18bc9c",
          background: "#33d7b6"
        }
      },
      fontColor: "white"
    },
    requirements: {
      color: {
        border: "#f39c12",
        background: "#f39c12",
        highlight: {
          border: "#f39c12",
          background: "#ffb62e"
        }
      },
      fontColor: "white"
    },
    prohibitions: {
      color: {
        border: "#e74c3c",
        background: "#e74c3c",
        highlight: {
          border: "#e74c3c",
          background: "#ff6857"
        }
      },
      fontColor: "white"
    }
  },
  physics: {
    barnesHut: {
      enabled: true,
      gravitationalConstant: -5000,
      centralGravity: 0,
      springLength: 200,
      springConstant: 0.5,
      damping: 0.3
    }
  }
};

var network = new vis.Network(container, data, options);

var canvasOptions =
  '<div id="options" class="options">' +
    '<a href="' + licenseurl + '" target="_blank" class="btn btn-default" ' +
    'title="Download RDF">' +
      '<i class="fa fa-download fa-lg"></i>' +
    '</a>' +
    '<button id="show-prefixes" target="_blank" class="btn btn-default" ' +
    'title="Show prefixes" data-toggle="button" aria-pressed="false" autocomplete="off">' +
      '<i class="fa fa-code fa-lg"></i>' +
    '</button>' +
  '</div>' +
  '<div id="prefixes" class="well col-md-5">' +
       '<ul class="list-unstyled">' +
         '<li>PREFIX : &lt;http://wimmics.inria.fr/projects/licentia/licenses/&gt;</li>' +
         '<li>PREFIX l4lod: &lt;http://ns.inria.fr/l4lod/&gt;</li>' +
         '<li>PREFIX odrl: &lt;http://www.w3.org/ns/odrl/2/&gt;</li>' +
         '<li>PREFIX rdfs: &lt;http://www.w3.org/2000/01/rdf-schema#&gt;</li>' +
       '</ul>' +
   '</div>';

$(window).load(function() {
  $("#graph div.vis").append(
    canvasOptions
  );

  $("#show-prefixes").click(function(event){
    $("#prefixes").fadeToggle("slow");
  });
});