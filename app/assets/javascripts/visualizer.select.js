$(window).load(function() {
  $.get(apiLicensesPath, function(licenses) {
    licenses.sort(sortLicenses);

    var licensesContext = {options: licenses};
    template = Handlebars.compile($("#selector-template").html());
    html = template(licensesContext);
    $("#licenses-selector").append(html);

    $("#licenses-selector").chosen({
      no_results_text: "No results matching ",
      width: "100%"
    });

    $("select.form-control + .chosen-container .chosen-search").prepend(
      "<i class='fa fa-search fa-lg'></i>"
    );

    $(document).click(function(event) {
      var target = $($(event.target)[0]);

      var isTarget = $(target).is("a.chosen-single");
      var isTargetChild = $("a.chosen-single").find(target).get();

      if(isTarget || isTargetChild.length) {
        $("a.chosen-single").addClass("focus");
      } else {
        $("a.chosen-single").removeClass("focus");
      }
    });

    $("#loader").remove();
    $("#service").removeClass("hidden");
  });
});

$("#visualize").click(function(event) {
  event.preventDefault();

  if ($("#licenses-selector").val() === null) {
    alert("You must choose a license to visualize");
  } else {
    window.location.href = visualizerPath + $("#licenses-selector").val();
  }
});