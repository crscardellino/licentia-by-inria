/**
 * Licentia by INRIA
 * (c) 2014 Cristian Cardellino for INRIA
 *
 * Licensed under Apache License, Version 2.0
 * See LICENSE for more information.
 */

package models

object JsonFormats {
  import play.api.libs.json.Json

  // Generates Writes and Reads thanks to Json Macros
  implicit val conditionFormat = Json.format[Condition]
  implicit val licenseFormat = Json.format[License]
}
