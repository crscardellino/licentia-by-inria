/**
 * Licentia by INRIA
 * (c) 2014 Cristian Cardellino for INRIA
 *
 * Licensed under Apache License, Version 2.0
 * See LICENSE for more information.
 */

package models

case class License( id: String, /* Private id for the DB */
                    uri: String, /* RDF URI and Link to the License */
                    about: String, /* Human readable license version */
                    title: String, /* Human readable license name */
                    version: Option[String], /* Version of the license */
                    conditions: List[Condition] /* List of license conditions */
                    )