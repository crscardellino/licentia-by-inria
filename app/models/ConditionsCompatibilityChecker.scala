/******************************************************************************
*   Licentia by INRIA                                                         *
*                                                                             *
*   (c) 2014 Cristian Cardellino for INRIA                                    *
*                                                                             *
*   Licensed under the Apache License, Version 2.0 (the "License");           *
*   you may not use this file except in compliance with the License.          *
*   You may obtain a copy of the License at                                   *
*                                                                             *
*       http://www.apache.org/licenses/LICENSE-2.0                            *
*                                                                             *
*   Unless required by applicable law or agreed to in writing, software       *
*   distributed under the License is distributed on an "AS IS" BASIS,         *
*   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  *
*   See the License for the specific language governing permissions and       *
*   limitations under the License.                                            *
*                                                                             *
******************************************************************************/

package models

import spindle.io.parser.DflTheoryParser2

import scala.collection.JavaConversions._

import spindle.Reasoner
import spindle.core.dom.{Conclusion, ConclusionType}
import spindle.sys.Conf


class ConditionsCompatibilityChecker(val userConditions: List[Condition], val licenseConditions: License) extends AnyRef {
  Conf.initializeApplicationContext(null)

  private val clazzToModal: Map[String, String] = Map(
    "permission" -> "[-O]-",
    "prohibition" -> "[O]-",
    "requirement" -> "[O]"
  )

  private val modalToClazz: Map[String, String] = Map(
    "[-O]-" -> "permission",
    "[O]-" -> "prohibition",
    "[O]" -> "requirement"
  )

  private val negation: Map[Boolean, String] = Map(
    true -> "-",
    false -> ""
  )

  private def getDFSConditions(conditions: List[Condition], rName: String):
    List[(String, (String, String, String))] = {
    var index = 0

    for (condition <- conditions) yield {
      index += 1
      (rName + index,("=>", clazzToModal(condition.clazz), condition.rule))
    }
  }

  private def getTheoryRules: List[String] = {
    val uconditions = getDFSConditions(userConditions, "u").map(
      lv => lv._1 + ":" + lv._2._1 + lv._2._2 + lv._2._3
    )

    val lconditions = getDFSConditions(licenseConditions.conditions, "l").map(
        lv => lv._1 + ":" + lv._2._1 + lv._2._2 + lv._2._3
    )

    uconditions ++ lconditions
  }

  private def getConclusions: List[Conclusion] = {
    val reasoner: Reasoner = new Reasoner()

    val theory: StringBuilder = new StringBuilder()

    for (rule <- getTheoryRules) theory.append(rule).append('\n')

    reasoner.loadTheory(DflTheoryParser2.getTheory(theory.result(), null))

    reasoner.generateConclusionsWithTransformations

    reasoner.getConclusionsAsList.toList
  }

  def getConditionsCompatibility: (Boolean, Set[Condition]) = {
    val incompatibleConditions = (for (conclusion <- getConclusions
          if ConclusionType.DEFEASIBLY_NOT_PROVABLE.
            equals(conclusion.getConclusionType)) yield {

          val dfsModalString = conclusion.getLiteral.getMode.toString + negation(conclusion.getLiteral.isNegation)

          Condition(modalToClazz(dfsModalString),
                    conclusion.getLiteral.getName,
                    None)
    }).toSet

    if (incompatibleConditions.isEmpty)
      (true, userConditions.toSet -- licenseConditions.conditions.toSet)
    else {
      (false, incompatibleConditions -- licenseConditions.conditions.toSet)
    }
  }
}
