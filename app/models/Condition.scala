/**
 * Licentia by INRIA
 * (c) 2014 Cristian Cardellino for INRIA
 *
 * Licensed under Apache License, Version 2.0
 * See LICENSE for more information.
 */

package models

import java.lang.IllegalArgumentException
import java.util.NoSuchElementException

case class Condition(clazz: String, /* permission, prohibition or requirement */
                     rule: String, /* Rule for defeasible logic operations */
                     label: Option[String] /* Human readable condition name */
                     ) extends Ordered[Condition] {

  private val classOrder: Map[String, Int] =
    Map(
      "permission" -> 0,
      "requirement" -> 1,
      "prohibition" -> 2
    )

  override def equals(o: Any) = o match {
    case that: Condition => that.clazz.equals(this.clazz) && that.rule.equals(this.rule)
    case _ => false
  }

  def compare(that: Condition) =
    if (that.clazz.equals(this.clazz))
      this.rule.compareTo(that.rule)
    else
      try {
        classOrder(this.clazz) - classOrder(that.clazz)
      } catch {
        case e: NoSuchElementException => throw new IllegalArgumentException("Not recognized class.")
      }
}
