/**
 * Licentia by INRIA
 * (c) 2014 Cristian Cardellino for INRIA
 *
 * Licensed under Apache License, Version 2.0
 * See LICENSE for more information.
 */

package forms

case class UploadLicense(urn: String,
                         about: String,
                         title: String,
                         requirements: List[String],
                         permissions: List[String],
                         prohibitions: List[String])
