Licentia by INRIA
=================

Licensing framework web application for Linked Data.

(c) 2014 by Cristian Cardellino for INRIA

Licensed under Apache License v2.0.
See LICENSE file for more information.


## Server Requirements

- Java 7
- Apache Jena Fuseki server 1.1.1
- NGINX
- ruby

## Instructions

1. Create the directories:
	- `sudo mkdir -p /opt/licentia/conf /opt/fuseki/db /var/www/licentia`
	- `sudo cp ./licentia/production.conf /opt/licentia/conf`
2. Copy the licenses files to the `/var/www/licentia/licences` dir:
	- `sudo cp -r $HOME/licentia-by-inria/db/licenses /var/www/licentia`
3. Untar Apache Jena into `/opt/fuseki`:
	- `sudo tar xvf jena-fuseki-1.1.1-distribution.tar.gz -C /opt/fuseki`
4. Build Licentia and move it to the corresponding directory:
	- `cd ./licentia-by-inria`
	- `./activator clean compile`
	- `./activator dist`
	- `cd ./target/universal`
	- `unzip licentia-1.0.zip`
	- `sudo mv $HOME/licentia-by-inria/target/universal/licentia-1.0 /opt/licentia`
5. Create the Daemon users and assign the corresponding ownerships:
	- `sudo /usr/sbin/groupadd -r fuseki`
	- `sudo /usr/sbin/useradd -M -r -d / -s /sbin/nologin -c "FUSEKI Daemon" -g fuseki fuseki`
	- `sudo chown -R fuseki:fuseki /opt/fuseki/`
	- `sudo /usr/sbin/groupadd -r licentia`
	- `sudo /usr/sbin/useradd -M -r -d / -s /sbin/nologin -c "Licentia Server Daemon" -g licentia licentia`
	- `sudo chown -R licentia:licentia /opt/licentia/ /var/www/licentia/`
6. Copy the configuration and service files to the corresponding directories and enable and start/reload:
	- `sudo cp $HOME/licentia-by-inria/server_configs/nginx/* /etc/nginx`
	- `sudo cp $HOME/licentia-by-inria/server_configs/systemd/*.service /etc/systemd/system`
	- `sudo systemctl daemon-reload`
	- `sudo systemctl enable fuseki.service`
	- `sudo systemctl start fuseki.service`
	- `/opt/fuseki/jena-fuseki-1.1.1/s-put http://localhost:3030/licentia/data default $HOME/licentia-by-inria/db/licenses.ttl`
	- `sudo systemctl enable licentia.service`
	- `sudo systemctl start licentia.service`
	- `sudo systemctl enable nginx.service`
	- `sudo systemctl restart nginx.service`
7. Open the port:
    - `sudo /sbin/iptables -I INPUT -p tcp -m tcp --dport 80 -j ACCEPT`
