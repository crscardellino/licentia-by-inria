#!/usr/bin/env bash

find log*/ -type f -print0 | xargs -0 rm
