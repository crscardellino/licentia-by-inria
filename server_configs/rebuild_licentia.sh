#!/usr/bin/env bash

LICENTIA=$HOME/licentia-by-inria

echo "Starting to build new licentia module"
cd $LICENTIA

echo "Getting new source from repository"
git checkout master
git pull origin master

echo "Building new application"
./activator clean compile

echo "Building application distribution"
./activator dist

echo "Getting new application"
cd $LICENTIA/target/universal
unzip licentia-1.0.zip

echo "Stopping service"
sudo systemctl stop licentia.service
sudo systemctl stop fuseki.service

echo "Moving new application"
sudo rm -rf /opt/licentia/licentia-1.0
sudo mv $LICENTIA/target/universal/licentia-1.0 /opt/licentia

echo "Setting application rights"
sudo chown -R licentia:licentia /opt/licentia

echo "Renewing Services"
sudo cp $HOME/systemd/* /etc/systemd/system
sudo systemctl daemon-reload

echo "Starting service"
sudo systemctl start fuseki.service
sudo systemctl start licentia.service
