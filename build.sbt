import play.Play.autoImport._

name := """licentia"""

version := "1.0"

lazy val root = (project in file(".")).enablePlugins(PlayScala)

scalaVersion := "2.11.1"

libraryDependencies ++= Seq(
  jdbc,
  anorm,
  cache,
  ws,
  filters
)

resolvers += "Sonatype Snapshots" at "https://oss.sonatype.org/content/repositories/snapshots/"

libraryDependencies ++= Seq(
  "org.apache.jena" % "jena-arq" % "2.11.1",
  "net.sourceforge.owlapi" % "owlapi-distribution" % "3.5.0",
  "org.mindrot" % "jbcrypt" % "0.3m",
  "org.apache.jena" % "apache-jena-libs" % "2.11.2",
  "com.mohiva" %% "play-html-compressor" % "0.3"
)

includeFilter in (Assets, LessKeys.less) := "*.less"

excludeFilter in (Assets, LessKeys.less) := "_*.less"
